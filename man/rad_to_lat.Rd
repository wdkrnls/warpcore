% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/variograms.R
\name{rad_to_lat}
\alias{rad_to_lat}
\title{Convert radians into latitudes}
\usage{
rad_to_lat(r)
}
\arguments{
\item{r}{Numeric radians.}
}
\value{
Numeric latitudes.
}
\description{
90 degrees should be north pole (pi/2)
 0 should be equator             (0)
-90 degrees should be south pole (-pi/2)
}
\examples{
x = seq(-5*pi, 5*pi, by = pi/180)
plot(x, rad_to_lat(x), type = "l", ylim = c(-100, 100))
}
