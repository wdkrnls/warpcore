Warp Core
================

## About

The `warpcore` package aims to serve as a low-level reference
infrastructure for fitting nonlinear regression models using Maximum
Likelihood theory and then analyzing the results. By reference
infrastructure, I mean that I intend the code to prioritize clarity and
composability over execution speed. As a result, the package limits its
scope to the following goals:

1)  aiding in the transformation of likelihood functions into executable
    fitting functions
2)  using these likelihood functions to compute:

<!-- end list -->

  - modal parameter estimates (MLE)

<!-- end list -->

3)  Compute Fisher-Information based multivariate normal parameter
    sampling distributions to characterize uncertainty

<!-- end list -->

  - use the parameter sampling distributions to:
      - compute confidence intervals on parameters
      - test hypotheses about arbitrary contrasts

# Building Likelihood Functions

Building likelihood functions involves parameterizing functions.

``` r
library(warpcore)
log10b = parameterize(log, 
                      arg = c(y = "x"), 
                      fix = list(base = 10))
log10b(100)
```

    ## [1] 2

``` r
log5 = parameterize(log, arg = c(y = "x"),
                    fixing = list(base = 5))
log5(25)
```

    ## [1] 2

This concept is similar to currying as in `purrr::partial`. It may in
fact be equivalent. However, the focus here is on renaming arguments to
follow the nomenclature of your model.

Inspect these functions reveals the inputs that went into them:

``` r
e <- environment(log5)
pe <- parent.env(e)
ls(pe)
```

    ## [1] "arguments" "cargs"     "def"       "extra"     "fargs"     "fixing"   
    ## [7] "flook"     "fun"       "res"

``` r
pe$res
```

    ## function () 
    ## {
    ##     .e = environment()
    ##     make_function(args = fargs, body = quote({
    ##         .f = environment()
    ##         rename(.f, lookup = flook)
    ##         fun_call(fun, c(as.list(fixing), as.list(.e), as.list(.f)))
    ##     }), env = .e)
    ## }
    ## <environment: 0x559446456468>

``` r
pe$flook
```

    ##   x 
    ## "y"

Part of the scope of the package is making sure parameter optimization
can be done flexibly, so that different minimization algorithms can be
quickly tried. As a start, we aim to support three minimization
functions in R: nlm, optim, and genoud. The latter being a hybrid
genetic hill climber found in the package `rgenoud`.

# Transformation functions

A transformation function takes the optimizing vector and turns it into
a list.

## Covariance Functions

Here is a simple bivariate example illustrating how to start fitting
data.

``` r
library(mvtnorm)
tmu = matrix(c(5, 10))
tS  = matrix(c(3*3, -0.9*1*3, -0.9*1*3, 1*1), 2, 2)
X = rmvnorm(200, mean = tmu, sigma = tS)
plot(X)
grid()
```

![](readme_files/figure-gfm/unnamed-chunk-5-1.png)<!-- -->

The fitting framework the package encourages is different from the base
modeling packages as well as the newer tidy modeling packages. Instead
of relying on formulas, functions are constructed for computing both the
mean structure and the (auto) covariance structure.

``` r
# All mean functions take a model matrix and
# return a function of the parameters for that
# model matrix. It may be that nothing is done
# with this.
mean_structure <- function(X) {
  function(mu_x, mu_y) {
    matrix(c(mu_x, mu_y), ncol = 1)
  }
}

# All covariance functions take a response vector and
# optionally a distance matrix and return a covariance
# matrix. In the case where the covariance depends on
# none of them, these are simply ignored.
cov_structure  <- function(X, D) {
  function(sig_x, sig_y, rho) {
    co = rho*sig_x*sig_y
    matrix(c(sig_x*sig_x, co, co, sig_y*sig_y), 2, 2)
  }
}
```

These functions are then passed to a special likelihood function which
produces a fully specified model function which optionally allows for
specifying in turn an optimization function. This all sounds very
complicated, and is best understood by example.

``` r
model <- multivariate_gaussian(mean_structure, cov_structure)
fit <- model(X)
trans = make_trans(param = combined_arguments(mean_structure(X), cov_structure(X)),
                   spec = list(sig_x = exp, sig_y = exp, 
                               rho = sigmoid(low = -1, high = 1, rate = 1/2)))

result <- fit(trans, max.generations = 50, wait.generations = 4, print.level = 0,
              unif.seed = 42,
              int.seed = 42)
result$minimized$par
```

    ## [1]  5.063417686  9.990545649  1.036063846 -0.003903034 -5.603528951

``` r
library(tidyverse)

latex_sub_to_R_sub <- function(x) {
  ifelse(str_detect(x, "_"), 
         paste0(str_sub(x, end = -3), 
                "[", str_sub(x, start = -1, end = -1), "]"), 
                x)
}

cell <- ml_confidence_ellipses(result$mu, result$sigma, alpha = 0.01)

# TODO: try sorting facets by the extent of their correlation and optionally
# hiding some facets which are not particularly correlated.
cell %>% 
  mutate(xvar = str_replace(xvar, "sig", "sigma"),
         yvar = str_replace(yvar, "sig", "sigma"),
         xvar = latex_sub_to_R_sub(xvar),
         yvar = latex_sub_to_R_sub(yvar)) %>%
  ggplot(aes(x, y)) + 
  geom_path() + 
  facet_wrap(xvar ~ yvar, scales = "free", labeller = label_parsed) +
  theme(axis.title.x = element_blank())
```

![](readme_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

``` r
result[names(result) %in% c("AIC", "BIC")]
```

    ## $AIC
    ## [1] 1251.348
    ## 
    ## $BIC
    ## [1] 1267.839

It is often advantageous for a likelihood function to take parameters of
only among a limited range or set. Further, to get asymptotic confidence
intervals on the parameters the Hessian should be computed. This would
be slow to compute if done initially. So, it is advantageous for
multiple reasons to rerun the optimization starting from the fully
optimized parameters on the untransformed space.

## Unsafe covariance constructors

Not all combinations of correlation and variance parameters are valid.
According to this discussion, the angles between the correlations:

\[
\rho_{ij} = \text{arccos}(r_{ij})
\]

have to satisfy this inequality:

\[
\rho_{i_1 i_k} \le \sum_{l=1}^{k-1} \rho_{i_l i_{l+1}}
\]

<!-- TODO: would be cool to illustrate this fact. -->

Here is an example I stumbled upon where this is not true.

``` r
mu3 = c(8, -5, 7)
s1 = 4
s2 = 2
s3 = 1
r12 = 0.5
r13 = 0.9
r23 = -0.8

tS3 = matrix(c(s1*s1, r12*s1*s2, r13*s1*s3,
               r12*s1*s2, s2*s2, r23*s2*s3, 
               r13*s1*s3, r23*s2*s3, s3*s3), 
             nrow = 3, ncol = 3)

pracma::isposdef(tS3)
```

    ## [1] FALSE

The way to get around this problem is to specify only the upper
triangular decomposition of the matrix.

``` r
scv_fun = make_safe_covariance_function(nsigmas = 3)
cat("parameters:", "\n")
```

    ## parameters:

``` r
names(formals(args(scv_fun)))
```

    ## [1] "sig1"  "sig2"  "sig3"  "lam12" "lam13" "lam23"

``` r
cat("matrix\n")
```

    ## matrix

``` r
M3 = scv_fun(1, 1, 2, 0.1, -0.4, 0.6)
M3
```

    ##            [,1]      [,2]      [,3]
    ## [1,]  7.3890561 0.2718282 -1.087313
    ## [2,]  0.2718282 7.3990561  1.590969
    ## [3,] -1.0873127 1.5909691 55.118150

``` r
cat("Is positive definite?\n")
```

    ## Is positive definite?

``` r
pracma::isposdef(M3)
```

    ## [1] TRUE

## Future Goals

Compute the volume of the hyper-ellipsoid: smaller volumes should
reflect tighter parameter estimates for the same number of parameters.

<http://oaji.net/articles/2014/1420-1415594291.pdf>

The madness package provides an approach to forward autodiff in R.
Perhaps it could be supported in some way to compute high dimensional
likelihood functions more stably?

Flesh out examples of how to use this package might be used for fitting
cases involving distance based auto-covariance.

## Gotchas

You need to remember when a function takes many separate arguments or
one argument which is a vector.

Optimization requires the single vector argument.
