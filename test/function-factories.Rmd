---
title: "Debugging function factories"
output: html_notebook
---

A strange goal of this package is to construct functions with arguments named according to the goal of the learning model.

```{r}
library(magrittr)
library(warpcore)
```

```{r}
e <- environment(log5)
pe <- parent.env(e)
ls(pe)
```

# Calling relevant arguments

If the arguments are named according to the model, it should be pretty easy to avoid namespace collisions. In which case, it becomes interesting to make a wrapper around `do.call` which figures out which arguments are relevant and ignore the rest.

In the code below, `log` is called against 3 arguments, only 2 of which are in the package.

```{r}
warpcore:::fun_call(log, list(x = 3, base = 5, z = 3))
```

Compare this to what happens when `do.call` is used.

```{r eval=FALSE}
do.call(log, list(x = 3, base = 5, z = 3))
```

This feature really depends on making functions which reflect the model terminology. We do this using `parameterize`.

# Making functions

```{r}
warpcore:::make_function()
```


# Parameterizing function

It seems to me that parameterize should keep any required arguments by default rather than making you specify them.

```{r}
log %>%
  parameterize(arguments = c(x = "x"), 
               fixing = list(base = 3)) ->
  log3x

log %>%
  parameterize(arguments = c(y = "x"), 
               fixing = list(base = 3)) ->
  log3y
```

We can look inside the function environment:

```{r}
inspect_closure <- function(fun) {
  fun %>%
    environment() %>% 
    parent.env() -> pe
  cat("environment:\n")
  print(noquote(ls(pe)))
  cat("\n")
  for(item in ls(pe)) {
    cat(paste0(item, ":\n"))
    print(get(item, envir = pe))
    cat("\n")
  } 
}
```

The first shows:

```{r}
log3x %>% inspect_closure
```

And here is the other function:

```{r}
log3y %>% inspect_closure
```

For building the function to pass to the optimization function, we would want to produce a function taking a vector argument.

```{r}
warpcore::combined_arguments(log, sqrt)
```

Since `log` and `sqrt` share the argument `x`, it is implicitly assumed these must be the same value. This is why it is important to parameterize.

# Examples and diagnostics

```{r}
spatial_distance <- function(x1, x2 = x1) {
  fields::rdist.earth(x1, x2, miles = FALSE)
}

temporal_distance <- function(x1, x2 = x1) {
  fields::rdist(x1, x2)
}

spdist <- spatial_distance(loc[train,])
tmdist <- temporal_distance(madrid$time[train])

spatial_nugget   <- make_simple_nugget(spdist)
temporal_nugget  <- make_simple_nugget(tmdist)
spacetime_nugget <- make_interaction_nugget(spdist, tmdist)


for(nug in list("spatial_nugget", "temporal_nugget", "spacetime_nugget")) {
  print(nug)
  print(table(get(nug)))
  cat("\n")
}

```

Good. We see that spacetime nugget afflicts only a paucity of points.

```{r}
# Gaussian kernel
gaus_nug  =
  make_distance_covariance(kernel = Gaussian,
                           distances = list(spdist, tmdist),
                           nuggets = list(nug1 = spatial_nugget,
                                          nug2 = temporal_nugget,
                                          nug3 = spacetime_nugget),
                           kernel_params = list(sill = "alpha"))

# Matern kernel
mat_nug =
  make_distance_covariance(kernel = Matern,
                           distances = list(spdist, tmdist),
                           nuggets = list(nug1 = spatial_nugget,
                                          nug2 = temporal_nugget,
                                          nug3 = spacetime_nugget),
                           kernel_params = list(sill = "alpha",
                                                smoothness = "nu"))
```

Here is code that should work:

```{r}
dim(mat_nug(1, 1, 1, 1, 0, 0, 0))
```

Here is code to make sure the matrices are plausible covariance matrices.

```{r}
pracma::isposdef(gaus_nug(alpha = 1, # sill
                          beta1 = 1, beta2 = 1, # ranges
                          nug1 = 1, nug2 = 1, nug3 = 1)) # nuggets

pracma::isposdef(mat_nug(alpha = 1, nu = 5,
                         beta1 = 1, beta2 = 1,
                         nug1 = 1, nug2 = 1, nug3 = 1))
```

```{r}
par_matern <- parameterize(Matern, arguments = c("distance", nu = "smoothness"))
par_matern(1, 1)
```

```{r}
ls(environment(mat_nug))
```

```{r}
cat(environment(mat_nug)$pexpr)
```

```{r}
environment(mat_nug)$params
```
