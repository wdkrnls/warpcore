---
title: "Linear Models"
author: "Kyle Andrews"
output: html_notebook
---

How can warpcore be used to fit ordinary linear regression models?

There are four kinds of these models:

1. single variable, single response regression
2. single variable, multi response regression
3. multi variable, single response regression
4. multi variable, multi response regression

# Single Variable, Single Response

```{r}
set.seed(42)
x = runif(15, 0, 10)
u = 1*x + 3
e = rnorm(x, sd = 0.8)
y = u + e
plot(x, y)
grid()
```

The trend function and covariance function can be written easily.

```{r}
library(warpcore)
trend = function(x) {
  function(m, b) {
    matrix(m*x + b, nrow = 1)
  }
}

covar = function(y) {
  n = ncol(y)
  function(s2) {
    s2*diag(n)
  }
}
```


```{r}
X = matrix(x, nrow = 1)
Y = matrix(y, nrow = 1)
model = maximum_likelihood(Y)
fit   = model(mean_fun = trend(X), cov_fun = covar(Y))
lik   = log_likelihood(fit)
grid  = expand.grid(m = seq(0, 5, 1),
                    b = seq(0, 5, 1),
                    s2 = seq(0.5, 3, 0.5))
trans = make_trans(combined_arguments(trend(X), covar(Y)),
                   spec = list(s2 = exp))
start = explore_likelihood(grid, lik, trans)
init  = most_likely(start)
result = fit(trans, 
             optimizer = wrapped_nlm(p = unlist(init),
                                     print.level = 2))
```


```{r}
plot_evidence(lik, result$mu)(parameter = "m", limits = c(0.5, 2))
```

```{r}
plot_evidence(lik, result$mu)(parameter = "b", limits = c(3, 4.5))
```


```{r}
library(mvtnorm)
library(parallel)
nboot = 2000
#boot_est = vector("list", length = nboot)
started = proc.time()
boot_est =
mclapply(1:nboot, function(b) {
  cat("Beginning bootstrap: ", b, "\n")
  yb = with(as.list(result$mu), rmvnorm(1, mean = trend(X)(m, b), sigma = covar(Y)(s2)))
  # Estimate the parameters for this data set
  modelb <- maximum_likelihood(yb)
  fitb <- modelb(mean_fun = trend(X), 
                 cov_fun  = covar(yb))
  resultb <- suppressMessages(suppressWarnings(fitb(trans, optimizer = wrapped_nlm(p = result$minimized$estimate))))
  resultb$mu
}, mc.cores = 4)
stopped = proc.time()
stopped - started
```



```{r}
Boot = do.call(rbind, boot_est)
plot(x, y)
xs = seq(0, 10, 0.1)
for(b in 1:nrow(Boot)) {
  lines(xs, with(as.list(Boot[b,,drop=TRUE]), m*xs+b), 
        col = adjustcolor("blue", alpha = 0.01))
}
grid()
```

Now we can compute confidence intervals.


```{r}
hist(do.call(rbind, boot_est)[,"m"], 
     breaks = 40, 
     main = "Confidence in slope", 
     xlab = "m")
```

```{r}
plot(ecdf(x = do.call(rbind, boot_est)[,"m"]),
     main = "Confidence in slope", xlab = "m")
abline(h = c(0.025, 0.975), col = "red")
```

Compare this result to what you get from `lm`.

```{r}
confint(lm(y ~ x))
```

```{r}
plot(ecdf(x = do.call(rbind, boot_est)[,"b"]),
     main = "Confidence in intercept", xlab = "b")
abline(h = c(0.025, 0.975), col = "blue")
```

```{r}
plot(ecdf(x = do.call(rbind, boot_est)[,"s2"]),
     main = "Confidence in variance", xlab = expression(sigma^2))
abline(h = c(0.025, 0.975), col = "purple")
```

I would like to functionalize this so that it becomes very easy.

```{r}
#' General approach to performing parametric bootstrap
#' @param result MLResult
#' @param rdist Function taking result and sampling from it
#' @param fit Function taking the response and returning B 
#' @param nboot Integer(1) number of bootstrap samples.
#' @param loop Function.
#' @param ... Extra arguments.
#' @return Function -> List
parametric_bootstrap = function(result, rdist, fit) {
  function(nboot, loop = lapply, ...) {
    loop(1:nboot, function(b) {
      yb = fun_call(rdist, result$mu)
      resb <- suppressMessages(suppressWarnings(fit(response = yb, init = result$minimized$estimate)))
      resb$mu
    })
  }
}
```

```{r}
mvn_sample = function(m, b, s2) {
  rmvnorm(1, mean = trend(X)(m, b), sigma = covar(Y)(s2))
}

fit_model = function(response, init) {
  model = maximum_likelihood(response)
  fit   = model(trend(X), covar(y))
  fit(trans, optimizer = wrapped_nlm(p = init))
}

boot_est2 = parametric_bootstrap(result, mvn_sample, fit_model)(nboot = 10)
```

This doesn't work. It's also too complicated.

```{r}
plot_evidence(lik, result$mu)(parameter = "s2", limits = c(0, 3))
```

# Multi-variable, Single Response Regression

The most common case is this one.

```{r}
set.seed(42)
b  = 2
x1 = runif(15, 0, 10)
x2 = runif(15, 0, 10)
u = 1*x1 - 2*x2 + b
e = rnorm(x, sd = 0.8)
y = u + e

```

```{r}
plot(x1, y)
```

```{r}
plot(x2, y)
```

This is the sort of thing that could really benefit from have a marginal model plot.

However, in this case we can also make a plot with colors.

```{r}
plot(x1, x2, col = (c("red", "orange", "blue"))[findInterval(y, c(-17, -5, 5, 15))])
```

```{r}
library(warpcore)
trend = function(x) {
  m = ncol(x)
  n = nrow(x)
  function(m1, m2, b) {
    A = matrix(c(m1, m2, b), ncol = 1)
    cbind(x, rep(1, n)) %*% A
  }
}

covar = function(y) {
  n = ncol(y)
  function(s2) {
    s2*diag(n)
  }
}
```


```{r}
X = cbind(x1, x2)
Y = matrix(y)
model = maximum_likelihood(Y)
fit   = model(mean_fun = trend(X), cov_fun = covar(Y))
lik   = log_likelihood(fit)
grid  = expand.grid(m1 = seq(0, 5, 1),
                    m2 = seq(-5, 0, 1),
                    b = seq(0, 5, 1),
                    s2 = seq(0.5, 3, 0.5))
trans = make_trans(combined_arguments(trend(X), covar(Y)),
                   spec = list(s2 = exp))
start = explore_likelihood(grid, lik, trans)
init  = most_likely(start)
result = fit(trans, 
             optimizer = wrapped_nlm(p = unlist(init),
                                     print.level = 2))
```

Are we in the right ballpark?

```{r}
plot(fun_call(trend(X), result$mu), y)
```

And what does the evidence look like?

```{r}
evidence = plot_evidence(lik, result$mu)
evidence("m1", limits = c(0, 2))
```

```{r}
evidence("m2", limits = c(-3, -1))
```

```{r}
evidence("b", limits = c(0, 3))
```

Interestingly we see that our estimate of b is biased considerably below the truth.

```{r}
evidence("s2", limits = c(0, 5))
```

The variance is actually over estimated.

# Multi variable, multi response regression

Let's take the same data as before, but let's add another variable which
is known to be quite related to the first.

```{r}
y2 = 0.8*y*rnorm(y, mean = 1, sd = 0.2)
```

```{r}
plot(y, y2)
abline(a = 0, b = 0.8)
```

Now the trend function should return a matrix with two columns.

```{r}
trend = function(x) {
  m = ncol(x)
  n = nrow(x)
  function(m1, m2, b) {
    A = matrix(c(m1, m2, b), ncol = 1)
    res = cbind(x, rep(1, n)) %*% A
    cbind(res, res)
  }
}
```

The covariance matrix needs to return a distance matrix of size 2x2.

This makes me wonder, why can't I return a 1x1 matrix for the covariance
in the case of single response regression?

```{r}
covar = function(y) {
  n = ncol(y)
  function(s2, rho) {
    s2*matrix(c(1, rho, rho, 1), nrow = n, ncol = n)
  }
}
```


```{r}
X = cbind(x1, x2)
Y = cbind(y, y2)
model = maximum_likelihood(Y)
fit   = model(mean_fun = trend(X), cov_fun = covar(Y))
lik   = log_likelihood(fit)
grid  = expand.grid(m1 = seq(0, 5, 1),
                    m2 = seq(-5, 0, 1),
                    b = seq(0, 5, 1),
                    s2 = seq(0.5, 3, 0.5),
                    rho = seq(-1, 1, 0.5))
trans = make_trans(combined_arguments(trend(X), covar(Y)),
                   spec = list(s2 = exp, rho = sigmoid(-1.1, 1.1)))
start = explore_likelihood(grid, lik, trans)
init  = most_likely(start)
result = fit(trans, 
             optimizer = wrapped_nlm(p = unlist(init),
                                     print.level = 2))
```

```{r}
plot(fun_call(trend(X), result$mu)[,1], y)
points(fun_call(trend(X), result$mu)[,2], y2, col = "red")
```

```{r}
evidence = plot_evidence(lik, result$mu)
evidence("m1", limits = c(0, 2))
```

```{r}
evidence("m2", limits = c(-3, 0))
```

Considering this looks biased downwards, I'm wondering whether I'm fitting the model
I think I am.

```{r}
evidence("b", limits = c(0, 4))
```


```{r}
evidence("rho", limits = c(-1, 1))
```

