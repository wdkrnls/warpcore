---
title: "Streamlined Kriging Variance"
author: "Kyle Andrews"
output: html_notebook
---

In `correct-kriging-variance.Rmd` we figured out how to find
the kriging variability and sample from the fitted correlated
distribution. In this workbook, we streamline the process so
that it becomes easy to perform even if and when you are less
familiar with how the model works.

We'll start with the same model as before.

```{r}
library(warpcore)
import::from(pracma, isposdef)
f = function(x) -0.0579*x^4 + 1.11*x^3 - 6.845*x^2 + 14.1071*x + 2
x = seq(0, 10, 0.01)
z = matrix(f(x), nrow = 1)
plot(x, z, type = "l")
k = 8
xd = x[equidistant(x, k)]
zd = matrix(z[equidistant(x, k)], nrow = 1)
points(xd, zd, col = "red")
grid()
```

Fitting the model is already fairly streamlined. However, upon reflection
I have identified some modifications that would considerably streamline
the subsequent analysis of the results.

The first step is to define the distance and nugget matrices. No changes here.

```{r}
Dd = distance_matrix(xd, xd)
Nd = make_simple_nugget(Dd)
D  = distance_matrix(xd, x)
nd = make_simple_nugget(D)
```

The next step is to define the mean and covariance structures. These fundamentally
need to change into higher order functions so they can be reused in both training
and prediction situations. Furthermore, they need 

```{r}
mfun = make_constant_mean(nvars = 1, 
                          nobs = length(zd), 
                          fun = t, 
                          params = "mu")
covar = make_distance_covariance(distances = list(Dd), 
                                 nuggets = list(Nd), 
                                 nugget_params = "nug", 
                                 kernel = Gaussian,
                                 kernel_params = c(sill = "alpha"))
```

The code should become:

```{r eval=FALSE}
make_mfun = 
  make_constant_mean(nvars = 1,
                     fun = t, 
                     params = "mu")
# mfun = make_mfun(nobs = length(zd))
make_covar = 
  make_distance_covariance(nugget_params = "nug", 
                           kernel = Spherical,
                           kernel_params = c(sill = "alpha"))
# covar = make_covar(distances = list(Dd), nuggets = list(nug = Nd))
```

```{r}
model = maximum_likelihood(zd)
fit   = model(mfun, covar)
lik = log_likelihood(fit)
trans = make_trans(combined_arguments(mfun, covar),
                   default = identity,
                   spec = list(alpha = sigmoid(low = 1, high = 300),
                               beta  = sigmoid(low = 0.5, high = 10),
                               nug   = sigmoid(low = 0, high = 1e-5)))
```

I added a function to streamline the choosing of starting values for 
hill climbing optimizers.

```{r}
psurf = expand.grid(mu    = c(-10, 0, 10), 
                    alpha = inv_sigmoid(1, 300)(c(5, 10, 50, 100, 150, 200)), 
                    beta  = inv_sigmoid(0.5, 10)(c(1, 2.1, 3, 4, 5)),
                    nug   = inv_sigmoid(0, 1e-8)(1e-9))

starting.values = best_starting_values(psurf, log_lik = lik, trans = trans)
res = fit(trans, optimizer = wrapped_nlm(p = starting.values))
```

Building the training and test covariance matrices from the chosen
covariance should be automated so that all the appropriate nuggets 
are added and the appropriate kernel function is used.

<!-- TODO:
This will require modifying the covariance function to become
higher order:

1. first specify the parameters
2. then specify the distances and nuggets
3. finally return a function of the desired parameters
covariance function.
-->

```{r}
assign_all(res$mu)

K = Gaussian(Dd, range = beta, sill = alpha)
k = Gaussian(D,  range = beta, sill = alpha)

K = add_nuggets(K, Nd, nug)
k = add_nuggets(k, nd, nug)
```

Then the code could be reduced to:

```{r eval=FALSE}
K = fun_call(make_covar(Dd, nuggets = list(nug = Nd)), res$mu)
k = fun_call(make_covar(D,  nuggets = list(nug = nd)), res$mu)
```

Then there is the question of building the table of results.

```{r}
#' Produce a data frame of the kriging results.
#'
#' TODO: 
#' Ideal interface requires changing the ml_results object to contain
#' a generic mean structure object; not one tied to the training set.
#' 
#' TODO:
#' This matrix is constrained to single response variable.
#' Consider allowing multivariate.
#' @param residual Numeric training response data.
#' @param cov_train Matrix Numeric covariance of training data.
#' @param cov_pred Matrix Numeric covariance of predictions.
#' @return Numeric kriging predictions (to be added to mean structure).
#' @export
kriging_predictions <- function(residual, cov_train, cov_pred) {
  K = cov_train
  k = cov_pred
  r = residual
  weight= pd_solve(K) %*% k
  as.numeric(t(weight) %*% r)
}
```


```{r}
plot(x, mu + kriging_predictions(as.numeric(zd) - mu, K, k), type = "l")
lines(x, z, col = "red")
```

(8 x 8) . (8 x 8) . (8 x 1001) / (8 x 8)

```{r}
pracma::nearest_spd(kriging_classical_variance(alpha, K, k))
```

