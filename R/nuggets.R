
#' Return the matrix
#'
#' Consider running zapsmall on the distance matrices before evaluating them.
#' @param D Matrix pairwise location distances.
#' @return L Matrix(Logical) nugget/no nugget?
#' @export
make_simple_nugget <- function(D) {
  D == 0
}



same <- function(x, y, otherwise = vector(mode(x))) {
  if(identical(x, y)) x else otherwise
}


#' Build up a list from two or more entries.
#' @param x Item.
#' @param ... Extra items in list.
make_list <- function(x, ..., check = TRUE, compare = mode) {
  dots <- list(...)
  if(!is.list(x)) {
    x <- list(x)
  }
  xs <- c(x, dots)
  if(check) {
    check_same(xs, compare)
  }
  xs
}


check_same <- function(xs, fun = identity) {
  u <- unique(Map(fun, xs))
  n <- length(u)
  if(n > 1) {
    stop("check_same: ",
         n, " unique entries were found in the list. ",
         "Only one should have been!")
  }
}


#' Return the interaction between several nuggets
#' @param D1 Matrix pairwise location distances under first distance metric.
#' @param D2 Matrix pairwise location distances under second distance metric.
#' @param ... Extra Matrix pairwise location distances.
#' @export
make_interaction_nugget <- function(D1, D2, ..., check = FALSE) {
  Ds <- c(list(D1), list(D2), list(...))
  Ns <- Map(make_simple_nugget, Ds)
  if(check) {
    check_same(Ns, dim)
  }
  Reduce(`&`, Ns)
}
