---
title: "Parallel Matrix Inverse"
author: "Kyle Andrews"
output: html_notebook
---

<!-- NOTE:
This approach has been tabled for now. I have achieved a method
of inverting large matrices computationally efficiently, but at
the cost of additional round off error. This problem is more
pressing than faster inversion.
-->
I'm curious about finding ways of parallelizing the computation of an inverse
matrix so that I can handle very large data sets.



$$
\newcommand{\b}[1]{\mathbf{#1}}
$$

$$
\newcommand{\t}[1]{#1^{\intercal}}
$$

$$
\newcommand{\inv}[1]{#1^{-1}}
$$

# Ideas
## Inverse product

Suppose that:

$$
\Sigma = A B
$$

Then:

$$
\inv{\Sigma} = \inv{(A B)} = \inv{B} \inv{A}
$$

We know that:

$$
\Sigma = L \t{L}
$$

But also that:

$$
\t{(\inv{A})} = \inv{(\t{A})}
$$

## Conditional Distribution

The proof of the multivariate normal conditional distribution relies on the following
result for the inverse of a symmetric partitioned matrix:

$$
\begin{bmatrix}
A & C \\
\t{C} & B
\end{bmatrix}^{-1}
=
\begin{bmatrix}
\inv{A} + \inv{A} C \inv{D} \t{C} \inv{A} & -\inv{A} C \inv{D} \\
-\inv{D} \t{C} \inv{A} & \inv{D}
\end{bmatrix}
$$

where:

$$
D = B - \t{C} \inv{A} C
$$

I need to prove to myself that this works.

```{r}
K = cbind(c(2, 1), c(1, 2))
A = K[1,1,drop=FALSE]
B = K[2,2,drop=FALSE]
C = K[1, 2,drop=FALSE]
tC = t(C)
iA = pd_solve(A)
D = B - tC %*% iA %*% C
iD = pd_solve(D)
iK = rbind(cbind(iA + iA %*% C %*% iD %*% tC %*% iA, -iA %*% C %*% iD),
           cbind(-iD %*% tC %*% iA                 , iD))
iK2 = pd_solve(K)
all.equal(iK, iK2)
```

```{r}
library(warpcore)
K = cbind(c(2, 1), c(1, 2))
parted_pd_solve <- function(K, p = ceiling(n/2)) {
  n = ncol(K)
  a = 1:p
  b = (p+1):n
  A = K[a,a,drop=FALSE]
  B = K[b,b,drop=FALSE]
  C = K[a, b,drop=FALSE]
  tC = t(C)
  iA = pd_solve(A)
  D = B - tC %*% iA %*% C
  iD = pd_solve(D)
  rbind(cbind(iA + iA %*% C %*% iD %*% tC %*% iA, -iA %*% C %*% iD),
        cbind(-iD %*% tC %*% iA                 , iD))
}

iK  = parted_pd_solve(K)
iK2 = pd_solve(K)
iK3 = recursive_solve(K, chunks = 1)
all.equal(iK, iK2, iK3)
```

Let's get a slightly more complicated matrix to validate indices.

We can generate a positive definite matrix by multiplying any 
lower triangular matrix together with it's transpose.

```{r}
L = matrix(c(2, 1, -2, 0, -1, 0.5, 0, 0, 1), ncol = 3, nrow = 3)
K = L %*% t(L)
```


The recursive version of this algorithm is to split the matrix up into 
'chunks'. Each chunk is a square matrix starting from the top left corner.
However, each matrix becomes successively larger as the inverse is progressively
built up.

```{r}
#' Solve any non-singular matrix recursively by breaking
#' it up into chunks.
#' @param K Matrix(Numeric) positive definite matrix.
#' @param chunks Integer(1) number of B matrices.
#' @param solve Function used to solve the matrix.
recursive_solve = function(K, chunks = 1, solve = pd_solve) {
  n = ncol(K)
  m = nrow(K)
  stopifnot(m == n)
  if(chunks == 0) {
    return(solve(K))
  }
  stopifnot(chunks < n, chunks > 0)
  p = n %/% (chunks + 1)
  r = n %% (chunks + 1)
  a = 1:p
  A1 = K[a, a, drop=FALSE]
  iA1 = solve(A1)
  rm(a)
  chunk_inverse = function(iA, i) {
    if(i == chunks + 1) {
      return(iA)
    }
    j = i*p
    a = 1:j
    b = (j+1):(if(i == chunks) n else j + p)
    #cat("i:", i, "\n")
    #cat("a:", range(a), "\n")
    #cat("b:", range(b), "\n")
    B = K[b, b, drop=FALSE]
    C = K[a, b, drop=FALSE]
    tC = t(C)
    tCiA = tC %*% iA
    D = B - tCiA %*% C
    iD = solve(D)
    # This is the computation of the inverse. What you need to do is
    # take this inverse and plug it into the next iteration.
 
    iACiD = iA %*% C %*% iD
    Y = rbind(cbind(iA + iACiD %*% tCiA, -iACiD),
              cbind(-iD %*% tCiA       , iD))
    # cat("Y:", dim(Y), "\n")
    chunk_inverse(Y, i + 1)
  }
  chunk_inverse(iA1, 1)
}
```

```{r}
time_it <- function(expr) {
  start = Sys.time()
  result = expr
  end = Sys.time()
  change = end - start
  cat("> Took", round(change, 1), paste0(attr(change, "units"), ".\n"))
  result
}
```

```{r}
all.equal(recursive_solve(K, chunks = 2), recursive_solve2(K, chunks = 2))
```

```{r}
iK = time_it(pd_solve(K))
iK2 = time_it(parted_pd_solve(K))
iK3 = time_it(recursive_solve(K, chunks = 2))
#all.equal(iK, iK2, iK3)
```

```{r}
library(microbenchmark)
microbenchmark(pd_solve(K), parted_pd_solve(K), recursive_solve(K, chunks = 2), times = 20)
```


The question then becomes: how can I break appart the matrix such that each part can be solved
independently? This format seems to require the computation of $\inv{A}$ first. Then, $\inv{D}$
can be computed. Then the the results are combined to form the inverse of $\Sigma$, but
potentially more efficiently.

The more I look at it, the more I'm convinced it should work. I can just solve each little part
to form the larger part, and build up. It's not really a parallel algorithm, more like a merge
sort algorithm, but it still looks like it should work.

Is this more efficient?

```{r}
library(warpcore)
library(microbenchmark)
M = distance_matrix(1:3000)
```


```{r}
K = Gaussian(M, smoothness = 1.95, range = 10, sill = 5)
microbenchmark(pd_solve(K), 
               recursive_solve(K, chunks = 1), times = 4)
```

It takes 20 seconds on average to invert this matrix. Can I make it faster using this
decomposition?

```{r}
A = K[1:2500,1:2500]
B = K[2501:5000,2501:5000]
C = K[1:2500, 2501:5000]
tC = t(C)
iA = pd_solve(A)
D = B - tC %*% iA %*% C
iD = pd_solve(D)
```

```{r}

iK = rbind(cbind(iA + iA %*% C %*% iD %*% tC %*% iA, -iA %*% C %*% iD),
           cbind(-iD %*% tC %*% iA                 , iD))
iK2 = pd_solve(K)
```

```{r}
fields::image.plot(iK2)
```

```{r}
fields::image.plot(iK)
```

These matrices are very different. I must have a bug somewhere.

```{r}
iA2 = iK2[1:2500, 1:2500]

peek <- function(x, range = 1:min(min(m, n), 10), range.x = range, range.y = range, digits = 2) {
  n = ncol(x)
  m = nrow(x)
  round(x[range.x, range.y], digits)
}

peek(iA)
```

```{r}
peak(iA2)
```

```{r}
peak(iA, range = 2000:2010)
```

```{r}
peak(iA2, range = 2000:2010)
```

```{r}
peak(iA, range.x = 1:10, range.y = 30:40)
```

```{r}
peek(iA2, range.x = 1:10, range.y = 30:40)
```

```{r}
library(warpcore)
set.seed(42)
M = distance_matrix(rnorm(600))
K = Gaussian(M, range = 30, sill = 10, smoothness = 2)
diag(K) <- diag(K) + 0.1
rcond(K)
kappa(K)
```


```{r}
library(parallel)
mclapply(0:10, function(i) {
  capture.output(invisible(time_it(recursive_solve(K, chunks = i))))
}, mc.cores = 2) -> runtimes
```

```{r}
peek(K)
```

```{r}
time_it(recursive_solve(K, chunks = 0))
```

Ideally, my goal would be to find a way to do this recursively. If I could somehow have
a promise that would do all of the 

The way the conditional distribution is found suggests a way of parallelizing the
computation of the inverse of a large positive definite square matrix.

My idea is to compute the inverse recursively much in the same way as merge sort:

1. break the matrix into two approximately equal size chunks.
2. find the inverse for each chunk
3. use the conditional result
